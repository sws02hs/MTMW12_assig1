# Calculate the Taylor series of exp and compare with numpy.exp

import numpy as np
import matplotlib.pyplot as plt

def expTaylorSeries(x, N):
    """Calculate the first N terms of the Taylor series of exp(x) and compare
    with the numpy exp. Returns an array of the errors for increasing numbers
    of terms up to N"""

    sum = 1.0
    error = np.zeros(N)
    expx = np.exp(x)
    for i in range(1,N):
        sum += x**i/np.math.factorial(i)
        error[i-1] = sum - np.exp(x)
        print('i = ', i, ' sum = ', sum, ', exp(',x,') = ', expx,
              ' difference = ', error[i-1])
    return error

def main():
    N = 50
    taylorErrors = expTaylorSeries(x=2, N=N)
    plt.semilogy(range(1,N+1), abs(taylorErrors), 'k')
    plt.ylabel('absolute value of error')
    plt.xlabel('number of terms of Taylor series')
    plt.ylim([1e-16,10])
    plt.savefig('expTaylorError.pdf')
    plt.show()
    
    print(expTaylorSeries(x=3, N=29))
    print(expTaylorSeries(x=3, N=2))

main()

