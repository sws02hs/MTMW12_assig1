# Implement and test a 1-point Guassian quadrature function

# import numerical library
import numpy as np

def GaussQuad(f, a, b, N = 100):
    """Integrate function f between a and b using 1-point Gaussian quadrature
    with N intervals. Return the integral"""

    # ensure that the limits are single precision
    a = np.float32(a)
    b = np.float32(b)

    # The Resolution
    dx = np.float32((b - a)/N)
    # Initialise the integral
    S = np.float32(0.0)

    # Sum contribution from each interval
    for i in range(N):
        S += np.float32(f(a + (i+0.5)*dx))

    S *= dx
    return S

def testGaussQuad():
    """Tesing of function GaussQuad to make sure that it integrates a linear
    function exactly"""
    
    # Define a linear function and the integration limits
    lin = lambda x: 3*x-2
    a = 0.
    b = 1.
    # The exact and numerical integral for various resolutions
    exact = 1.5*b**2 - 2*b - 1.5*a**2 + 2*a
    print("Testing GaussQuad for a linear function")
    print("N    Exact   Quadrature   Error (%)")
    for n in [1, 2, 3, 4]:
        numInt = GaussQuad(lin, a, b, n)
        print(n, exact, numInt, 100*(numInt - exact)/exact)

testGaussQuad()

# Numerical (N) and analytic (A) integrals of e^x
def main():
    """Integrate exp(x) between 0 and 1 using 1-point Gaussian quadrature and
    compare with analytic solution"""
    print("\nN    Exact   Quadrature     Error (%)")
    # Analytic solution
    expA = np.exp(1) - np.exp(0)
    for n in [2, 20, 200, 2000]:
        # numerical solution
        expN = GaussQuad(np.exp, 0, 1, n)
        error = 100*(expN - expA)/expA
        print(n, expA, expN, error)

main()

